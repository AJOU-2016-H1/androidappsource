package co.saru.familyyam;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by saru on 2016. 8. 16..
 */
public class videoListViewAdapter extends BaseAdapter {
    String flag;
    private ArrayList<ListViewItem> listViewItemList = new ArrayList<ListViewItem>() ;
    private ArrayList<ListViewItem> listTypeList = new ArrayList<ListViewItem>() ;


    // ListViewAdapter의 생성자
    public videoListViewAdapter() {

    }

    // Adapter에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return listViewItemList.size() ;
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        // "listview_item" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item_video, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        ViewHolder holder = new ViewHolder();
        holder.bubble_speech_ImageView = null;




        holder.bubble_speech_ImageView = (ImageView) convertView.findViewById(R.id.bubble_speech_ImageView);

        TextView titleTextView = (TextView) convertView.findViewById(R.id.video_item_textview) ;
        if(listTypeList.get(position).getTitle().equals("p")){
            holder.bubble_speech_ImageView.setImageResource(R.drawable.speech_bubble_right);
            titleTextView.setPadding(150, 0, 0, 0);

        }
        else{
            holder.bubble_speech_ImageView.setImageResource(R.drawable.speech_bubble_left);
            titleTextView.setPadding(350,0,0,0);
        }





        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        ListViewItem listViewItem = listViewItemList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        titleTextView.setText(listViewItem.getTitle());


        convertView.setTag(holder);
        return convertView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return listViewItemList.get(position) ;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem( String title, String type) {
        ListViewItem item = new ListViewItem();
        ListViewItem item2 = new ListViewItem();
        item.setTitle(title);
        item2.setTitle(type);
        listViewItemList.add(item);
        listTypeList.add(item2);
    }

    // 뷰홀더 생성
    private static class ViewHolder{
        ImageView bubble_speech_ImageView;
    }
}

