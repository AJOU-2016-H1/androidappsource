package co.saru.familyyam;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;


/**
 * Created by saru on 2016. 7. 28..
 */
public class record extends Activity implements SurfaceHolder.Callback {

    Button myButton;
    MediaRecorder mediaRecorder;
    SurfaceHolder surfaceHolder;
    boolean is_recording;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);


        is_recording = false;

        mediaRecorder = new MediaRecorder();
        initMediaRecorder();

        setContentView(R.layout.record);

        SurfaceView myVideoView = (SurfaceView) findViewById(R.id.videoview);
        surfaceHolder = myVideoView.getHolder();
        surfaceHolder.addCallback(this);

        myButton = (Button) findViewById(R.id.mybutton);
        myButton.setOnClickListener(myButtonOnClickListener);
    }

    private Button.OnClickListener myButtonOnClickListener = new Button.OnClickListener() {

        public void onClick(View arg0) {
            if (is_recording) {

                mediaRecorder.stop();
                mediaRecorder.release();
                finish();
            } else {
                mediaRecorder.start();
                is_recording = true;
                myButton.setText("녹화중지");
            }
        }
    };

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        prepareMediaRecorder();
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }

    private void initMediaRecorder() {

        //폴더 있는지 먼저 확인
        File file = new File(Environment.getExternalStorageDirectory()+"/DCIM/familyYam/");
        //없으면 만듬
        if(!file.exists()){
            file.mkdir();
        }



        Camera camera = Camera.open(1);
        camera.setDisplayOrientation(0);
        camera.unlock();
        mediaRecorder.setCamera(camera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        CamcorderProfile camcorderProfile_HQ = CamcorderProfile
                .get(CamcorderProfile.QUALITY_480P);
        mediaRecorder.setProfile(camcorderProfile_HQ);
        String filename = "/"+getDateString() + ".mp4";
        mediaRecorder.setOutputFile(file + filename);
        mediaRecorder.setMaxDuration(300000); // �ִ� �ð��� 300�ʷ� �����Ѵ�.
        mediaRecorder.setMaxFileSize(500000000); // �ִ� ����  ũ�⸦ 500MB�� �����Ѵ�.


/*

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        File file = new File(localPath + "/DCIM/Camera/video.mp4");
        try {
            HttpClient client = new DefaultHttpClient();
            String postURL = "https://test-api.kidsyam.com:3443/testapi/upload";
            HttpPost post = new HttpPost(postURL);
            FileBody bin = new FileBody(file);
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("PhotoData", bin);
            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                Log.i("RESPONSE", EntityUtils.toString(resEntity));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

*/



    }

    private void prepareMediaRecorder() {
        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 현재시간 받기
    public String getDateString()
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.KOREA);
        String str_date = df.format(new Date());

        return str_date;
    }



}
