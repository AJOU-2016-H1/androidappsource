package co.saru.familyyam;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class main extends TabActivity {

    int videoLength;
    final int REQUEST_CODE_VOICE_RECORD= 1;
    final int REQUEST_CODE_POPUP= 2;
    final String parentURL="https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=parent&Type=V";
    final String childURL="https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=kid&Type=V";
    final String voiceURL="https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=parent&Type=A";
    private int VideoLength =0;
    private int VoiceLength =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); // 상태바 없에기
        setContentView(R.layout.main_view);





        final TabHost tabHost = getTabHost();

        // 버튼 눌렸을때 안눌렸을때 이미지 추가할려면 아래 2줄처럼.
        ImageView tab_button_1 = new ImageView(this);
        tab_button_1.setImageResource(R.drawable.tab_button_1);
        ImageView tab_button_2 = new ImageView(this);
        tab_button_2.setImageResource(R.drawable.tab_button_2);
        ImageView tab_button_3 = new ImageView(this);
        tab_button_3.setImageResource(R.drawable.tab_button_3);

        // 시작하기전에 동영상 목록 몇개 있나 확인.

        //폴더 있는지 먼저 확인
        File familyYamFile = new File(Environment.getExternalStorageDirectory()+"/DCIM/familyYam/");
        //없으면 만듬
        if(!familyYamFile.exists()){
            familyYamFile.mkdir();
        }

        String startpath = Environment.getExternalStorageDirectory() + "/DCIM/familyYam/";
        File startfile = new File(startpath);
        File[] startfiles = startfile.listFiles();
        setVideoLength(startfiles.length);

        // 시작하기전에 음성목록 몇개 있나 확인.

        //폴더 있는지 먼저 확인
        File familyYamVoiceFile = new File(Environment.getExternalStorageDirectory()+"/DCIM/familyYamVoice/");
        //없으면 만듬
        if(!familyYamVoiceFile.exists()){
            familyYamVoiceFile.mkdir();
        }

        String startVoicepath = Environment.getExternalStorageDirectory() + "/DCIM/familyYamVoice/";
        File startVoicefile = new File(startVoicepath);
        File[] startVoicefiles = startVoicefile.listFiles();
        setVoiceLength(startVoicefiles.length);



        TabHost.TabSpec tabSpecTab1 = tabHost.newTabSpec("TAB1").setIndicator(tab_button_1);
        tabSpecTab1.setContent(R.id.tab1);
        tabHost.addTab(tabSpecTab1);

        TabHost.TabSpec tabSpecTab2 = tabHost.newTabSpec("TAB2").setIndicator(tab_button_2);
        tabSpecTab2.setContent(R.id.tab2);
        tabHost.addTab(tabSpecTab2);

        TabHost.TabSpec tabSpecTab3 = tabHost.newTabSpec("TAB3").setIndicator(tab_button_3);
        tabSpecTab3.setContent(R.id.tab3);
        tabHost.addTab(tabSpecTab3);

        tabHost.setCurrentTab(0);


        //showVoiceRecordList();
        startVoiceRecord();
        ListView videolv = (ListView) findViewById(R.id.videoListView);
        ListView voicelv = (ListView) findViewById(R.id.voiceListView);
        ListView kidsyamlv = (ListView) findViewById(R.id.kidsyamInfoListView);
        showVideoList(videolv);
        showVoiceList(voicelv);
        startRecord();
        showKidsyaminfoList(kidsyamlv);




    }

       //녹화 및 영상저장
    public void startRecord(){
        Button a = (Button)findViewById(R.id.startRecordButton);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recordIntend = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                Uri fileUri = getOutputMediaFileUri("familyYam", ".mp4"); // create a file to save the image
                // 먼저 선언한 intent에 해당 file 명의 값을 추가로 저장한다.
                recordIntend.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
                startActivityForResult(recordIntend, REQUEST_CODE_POPUP);


            }
        });
    }




    //녹음 시작.
    public void startVoiceRecord(){
        Button a = (Button)findViewById(R.id.startVoiceRecordButton);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recordIntent = new Intent(
                        MediaStore.Audio.Media.RECORD_SOUND_ACTION);

                startActivityForResult(recordIntent, REQUEST_CODE_VOICE_RECORD);
            }
        });
    }

    //녹음 파일 복사.
    private void copyFile(String fileName, Uri uri) throws IOException {


        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
            fis = new FileInputStream(fileName);
            fos = new FileOutputStream("/storage/emulated/0/DCIM/familyYamVoice/"+timeStamp+".mp3");
            byte[] buffer = new byte[1024];
            int readcount = 0;
            while((readcount=fis.read(buffer)) != -1) {
                fos.write(buffer, 0, readcount);    // 파일 복사
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            fis.close();
            fos.close();
        }
    }

    //녹음 이전파일 삭제, 녹음 저장 물어보기 , 동영상 저장 물어보기.
    @Override protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_VOICE_RECORD:
                if (resultCode == Activity.RESULT_OK) {
                    // Sound recorder does not support EXTRA_OUTPUT
                    Uri uri = data.getData();
                    try {
                        String filePath = getAudioPathFromUri(uri);
                        copyFile(filePath, uri);
                        getContentResolver().delete(uri, null, null);
                        (new File(filePath)).delete();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

                String newVoicePath = Environment.getExternalStorageDirectory() + "/DCIM/familyYamVoice/";
                File newVoiceFile = new File(newVoicePath);
                File[] newVoiceFiles = newVoiceFile.listFiles();

                Log.d("####getVoiceLength", String.valueOf(getVoiceLength()));
                Log.d("###newVoiceFiles.length", String.valueOf(newVoiceFiles.length));


                if(getVoiceLength()<newVoiceFiles.length) {
                    setVoiceLength(newVoiceFiles.length);
                    AlertDialog.Builder dialog = new AlertDialog.Builder(main.this);
                    dialog.setTitle("전송하시겠습니까?");
                    dialog.setCancelable(false);
                    // OK 버튼 이벤트
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //영상전송

                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);

                            String path = Environment.getExternalStorageDirectory() + "/DCIM/familyYamVoice/";
                            File file = new File(path);
                            File[] files = file.listFiles();
                            String filename = files[files.length - 1].getName();
                            String newestPath = Environment.getExternalStorageDirectory() + "/DCIM/familyYamVoice/" + filename;
                            File newestfile = new File(newestPath);
                            try {
                                String Creator = "parent";
                                String Title = newestfile.getName();
                                String Message = "parent_message";

                                HttpClient client = new DefaultHttpClient();
                                String postURL = "https://test-api.kidsyam.com:3443/test/upload?Creator=" + Creator + "&Title=" + Title + "&Message=" + Message + "&Type=A";
                                HttpPost post = new HttpPost(postURL);
                                FileBody bin = new FileBody(newestfile);
                                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                                reqEntity.addPart("PhotoData", bin);
                                post.setEntity(reqEntity);
                                HttpResponse response = client.execute(post);
                                HttpEntity resEntity = response.getEntity();
                                if (resEntity != null) {
                                    Log.i("RESPONSE@@@@", EntityUtils.toString(resEntity));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            voiceListViewAdapter adapter = new voiceListViewAdapter();
                            ListView lv = (ListView)findViewById(R.id.voiceListView);
                            lv.setAdapter(adapter);
                            showVoiceList(lv);
                            adapter.notifyDataSetChanged();

                        }
                    });
                    // Cancel 버튼 이벤트
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    dialog.show();
                }



            case REQUEST_CODE_POPUP:

                String newPath = Environment.getExternalStorageDirectory() + "/DCIM/familyYam/";
                File newFile = new File(newPath);
                File[] newFiles = newFile.listFiles();

                if(getVideoLength()<newFiles.length) {
                    setVideoLength(newFiles.length);

                    AlertDialog.Builder dialog = new AlertDialog.Builder(main.this);
                    dialog.setTitle("전송하시겠습니까?");
                    dialog.setCancelable(false);
                    // OK 버튼 이벤트
                    dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //영상전송

                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);

                            String path = Environment.getExternalStorageDirectory() + "/DCIM/familyYam/";
                            File file = new File(path);
                            File[] files = file.listFiles();
                            String filename = files[files.length - 1].getName();
                            String newestPath = Environment.getExternalStorageDirectory() + "/DCIM/familyYam/" + filename;
                            File newestfile = new File(newestPath);
                            try {
                                String Creator = "parent";
                                String Title = newestfile.getName();
                                String Message = "parent_message";

                                HttpClient client = new DefaultHttpClient();
                                String postURL = "https://test-api.kidsyam.com:3443/test/upload?Creator=" + Creator + "&Title=" + Title + "&Message=" + Message + "&Type=V";
                                HttpPost post = new HttpPost(postURL);
                                FileBody bin = new FileBody(newestfile);
                                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                                reqEntity.addPart("PhotoData", bin);
                                post.setEntity(reqEntity);
                                HttpResponse response = client.execute(post);
                                HttpEntity resEntity = response.getEntity();
                                if (resEntity != null) {
                                    Log.i("RESPONSE@@@@", EntityUtils.toString(resEntity));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            videoListViewAdapter adapter = new videoListViewAdapter();
                            ListView lv = (ListView)findViewById(R.id.videoListView);
                            lv.setAdapter(adapter);
                            showVideoList(lv);
                            adapter.notifyDataSetChanged();

                        }
                    });
                    // Cancel 버튼 이벤트
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    dialog.show();
                }

        }
    }

    //녹음 위치 가져오기.
    private String getAudioPathFromUri(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int index = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
        Log.d("@@@@@@",cursor.getString(index));
        return cursor.getString(index);
    }


    //파일 저장 경로 설정.
    private Uri getOutputMediaFileUri(String location, String type){
        // 아래 capture한 사진이 저장될 file 공간을 생성하는 method를 통해 반환되는 File의 URI를 반환
        return Uri.fromFile(getOutputMediaFile(location, type));
    }

   //파일 저장 경로 설정.
    private File getOutputMediaFile(String location, String type){

        // 외부 저장소에 이 App을 통해 촬영된 사진만 저장할 directory 경로와 File을 연결
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()+ "/DCIM/"+location+"/");
        if (! mediaStorageDir.exists()){ // 해당 directory가 아직 생성되지 않았을 경우 directory를 생성한다.
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // 시간으로 FILE명 생성
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date());
        File mediaFile;

            mediaFile = new File(mediaStorageDir.getPath() + File.separator + timeStamp + type);
        Log.d("@@@@@", mediaFile.toString());


        return mediaFile;
    }


/*
// 키즈얌에서 문자 받기
   public String getKidsYamMessage(){
       String URL = "https://api.kidsyam.com/common/GetLoginInfo?Id=gorao_ri@naver.com&Pw=tmddus4141";
       getKidsyamInfo getkyinfo = new getKidsyamInfo();
       String output = getkyinfo.previewChildInfo(getkyinfo.getChildName(getkyinfo.getKidsYamMessage(URL)), getkyinfo.getMapIdxList(getkyinfo.getKidsYamMessage(URL)));
       return output;
   }

*/

    //리스트뷰 형식의 비디오 리스트 보여줌
    public void showVideoList(ListView lv){

        final showChildRecordList SVL = new showChildRecordList();

        ListView listview ;
        final videoListViewAdapter adapter;

        // Adapter 생성
        adapter = new videoListViewAdapter();

        // 리스트뷰 참조 및 Adapter달기
        listview = lv;
        listview.setAdapter(adapter);



        // 각각 배열에 때려박기.
        JSONArray jAr1 = null;
        try {
            jAr1 = new JSONArray(SVL.getJsonMessage(parentURL));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] list1 = new String[jAr1.length()] ;

        for(int i=0; i< list1.length ;i++)
        {
            list1[i] = SVL.output2(SVL.getJsonMessage(parentURL), "regdate",i);
        }

        JSONArray jAr2 = null;
        try {
            jAr2 = new JSONArray(SVL.getJsonMessage(childURL));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] list2 = new String[jAr2.length()] ;

        for(int i=0; i< list2.length ;i++)
        {
            list2[i] = SVL.output2(SVL.getJsonMessage(childURL), "regdate",i);
        }


        int m=0;
        int p=0;
        int n=0;


        final String[] list = new String[list1.length+list2.length];
        // 아이의 동영상이 더 많을 때
        if(list1.length<list2.length)
        {
            for(n=0 ; m<list1.length; n++){
                if(list1[m].compareTo(list2[p])>0){
                    list[n] = list1[m]+" p "+m;
                    m++;
                }
                else{
                    list[n] = list2[p]+" c "+p;
                    p++;
                }
            }

            for(int o=n ; o<list.length;o++){
                list[o] = list2[p]+" c "+p;
                p++;
            }
        }

        // 부모의 동영상이 더 많을때.
        else
        {
            for(n=0 ; m<list2.length; n++){
                if(list2[m].compareTo(list1[p])>0){
                    list[n] = list2[m]+" c "+m;
                    m++;
                }
                else{
                    list[n] = list1[p]+" p "+p;
                    p++;
                }
            }

            for(int o=n ; o<list.length;o++){
                list[o] = list1[p]+" p "+p;
                p++;
            }
        }


        // 배열 합쳐진걸 어뎁터에 추가. 나중에 만든게 맨 밑으로감
        for(int j=0; j<list.length;j++){
            if(list[list.length - j - 1].split(" ")[2].equals("p")) {
                adapter.addItem(list[list.length - j - 1],"p");
            }
            else{
                adapter.addItem(list[list.length - j - 1],"c");
            }

        }

        //눌렀을때 비디오 실행.
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent playvideo = new Intent(Intent.ACTION_VIEW);
                if(list[list.length - position - 1].split(" ")[2].equals("p")){
                    playvideo.setDataAndType(Uri.parse(SVL.output2(SVL.getJsonMessage(parentURL), "path", Integer.parseInt(list[list.length - position - 1].split(" ")[3]))), "video/mp4");
                    // Log.d("asdfasdfasdf", playvideo.toString());
//                    startActivity(playvideo);
                }

                else{
                    playvideo.setDataAndType(Uri.parse(SVL.output2(SVL.getJsonMessage(childURL), "path", Integer.parseInt(list[list.length - position - 1].split(" ")[3]))), "video/mp4");
                    //  Log.d("asdfasdfasdf",playvideo.toString());
                    //                  startActivity(playvideo);
                }
                startActivity(playvideo);


            }

        }) ;
    }



    //리스트뷰 형식의 녹음 리스트 보여줌
    public void showVoiceList(ListView lv){

        final showChildRecordList SVL = new showChildRecordList();

        ListView listview ;
        final voiceListViewAdapter adapter;

        // Adapter 생성
        adapter = new voiceListViewAdapter() ;

        // 리스트뷰 참조 및 Adapter달기
        listview = lv;
        listview.setAdapter(adapter);


        // 각각 배열에 때려박기.
        JSONArray jAr1 = null;
        try {
            jAr1 = new JSONArray(SVL.getJsonMessage(voiceURL));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] list = new String[jAr1.length()] ;

        for(int i=0; i< list.length ;i++)
        {
            list[i] = SVL.output2(SVL.getJsonMessage(voiceURL), "regdate",i);
        }

        int m=0;
        int p=0;
        int n=0;



        // 배열 합쳐진걸 어뎁터에 추가. 나중에 만든게 맨 밑으로감
        for(int j=0; j<list.length;j++){
                adapter.addItem(list[j]);

        }

        //눌렀을때 녹음목록실행.
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
            Intent playvideo = new Intent(Intent.ACTION_VIEW);

                Log.d("######", Uri.parse(SVL.output2(SVL.getJsonMessage(voiceURL), "path", position)).toString());
                  playvideo.setDataAndType(Uri.parse(SVL.output2(SVL.getJsonMessage(voiceURL), "path", position)), "audio/mp3");

                startActivity(playvideo);

            }

        }) ;
    }




    public void showKidsyaminfoList(ListView lv){
        ListView listview ;
        final kidsyaminfoListViewAdapter adapter;

        final getKidsyamInfo GKI = new getKidsyamInfo();

        // Adapter 생성
        adapter = new kidsyaminfoListViewAdapter() ;
        // 리스트뷰 참조 및 Adapter달기
        listview = lv;
        listview.setAdapter(adapter);

        getKidsyamInfo gkyi = new getKidsyamInfo();
        String inputByJson4 = gkyi.getKidsYamMessage("https://api.kidsyam.com/common/GetLoginInfo?Id=gorao_ri@naver.com&Pw=tmddus4141");
        gkyi.previewChildInfo(gkyi.getChildName(inputByJson4),gkyi.getMapIdxList(inputByJson4),gkyi.getImageList(inputByJson4));
        String[] a = gkyi.getNameList();
        String[] b = gkyi.getSizeList();
        String[] c = gkyi.getStickerList();
        String[] d = gkyi.getr_stickerList();
        String[] e = gkyi.getImageList();

        // 배열 합쳐진걸 어뎁터에 추가. 나중에 만든게 맨 밑으로감
        for(int j=0; j<a.length;j++){
            adapter.addItem("이름 : "+a[j],"Size : "+b[j],c[j],d[j],e[j]);

        }




    }





    public void setVideoLength(int videoLength){
        this.VideoLength = videoLength;
    }

    public int getVideoLength(){
        return VideoLength;
    }

    public void setVoiceLength(int voiceLength){
        this.VoiceLength = voiceLength;
    }

    public int getVoiceLength(){
        return VoiceLength;
    }



}
