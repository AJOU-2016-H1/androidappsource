package co.saru.familyyam;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by saru on 2016. 8. 16..
 */
public class kidsyaminfoListViewAdapter extends BaseAdapter {
    String flag;
    private ArrayList<ListViewItem> nameList = new ArrayList<ListViewItem>() ;
    private ArrayList<ListViewItem> sizeList = new ArrayList<ListViewItem>() ;
    private ArrayList<ListViewItem> stickerList = new ArrayList<ListViewItem>() ;
    private ArrayList<ListViewItem> r_stickerList = new ArrayList<ListViewItem>() ;
    private ArrayList<ListViewItem> imageList = new ArrayList<ListViewItem>() ;



    // ListViewAdapter의 생성자
    public kidsyaminfoListViewAdapter() {

    }

    // Adapter에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return nameList.size() ;
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        // "listview_item" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item_kidsyaminfo, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        ViewHolder holder = new ViewHolder();
        holder.childImageView = null;

        TextView nameTextView = (TextView) convertView.findViewById(R.id.textview1) ;
        TextView sizeTextView = (TextView) convertView.findViewById(R.id.textview2) ;
        TextView stickerTextView = (TextView) convertView.findViewById(R.id.textview4) ;
        TextView r_stickerTextView = (TextView) convertView.findViewById(R.id.textview3) ;
        holder.childImageView = (ImageView)convertView.findViewById(R.id.childImage);


        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        ListViewItem listViewItem1 = nameList.get(position);
        ListViewItem listViewItem2 = sizeList.get(position);
        ListViewItem listViewItem3 = stickerList.get(position);
        ListViewItem listViewItem4 = r_stickerList.get(position);
        ListViewItem listViewItem5 = imageList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        nameTextView.setText(listViewItem1.getTitle());
       // sizeTextView.setText(listViewItem2.getTitle());
        stickerTextView.setText(listViewItem3.getTitle());
        r_stickerTextView.setText(listViewItem4.getTitle());

        if(listViewItem5.getTitle().equals("null")) {
            Drawable drawable = convertView.getResources().getDrawable(R.drawable.default_image);
            holder.childImageView.setImageDrawable(drawable);

        }

        else
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            //이미지 설정.
            try {
                URL url = new URL("http://img.kidsyam.com/PhotoData/" + listViewItem5.getTitle());
                URLConnection conn = url.openConnection();
                conn.connect();
                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                Bitmap bm = BitmapFactory.decodeStream(bis);
                bis.close();
                holder.childImageView.setImageBitmap(bm);
            } catch (Exception e) {
            }

        }

        convertView.setTag(holder);
        return convertView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return nameList.get(position) ;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem( String name, String size, String sticker, String r_sticker,String image) {
        ListViewItem item1 = new ListViewItem();
        ListViewItem item2 = new ListViewItem();
        ListViewItem item3 = new ListViewItem();
        ListViewItem item4 = new ListViewItem();
        ListViewItem item5 = new ListViewItem();


        item1.setTitle(name);
        item2.setTitle(size);
        item3.setTitle(sticker);
        item4.setTitle(r_sticker);
        item5.setTitle(image);
        nameList.add(item1);
        sizeList.add(item2);
        stickerList.add(item3);
        r_stickerList.add(item4);
        imageList.add(item5);

    }

    // 뷰홀더 생성
    private static class ViewHolder{
        ImageView childImageView;

    }
}

