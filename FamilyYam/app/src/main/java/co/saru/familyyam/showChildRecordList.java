package co.saru.familyyam;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saru on 2016. 7. 29..
 */
public class showChildRecordList extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final String URL="https://test-api.kidsyam.com:3443/test/getListKidsYam?Creator=kid&Type=V";

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recordlist);

        JSONArray jAr = null;
        try {
            jAr = new JSONArray(getJsonMessage(URL));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String[] list = new String[jAr.length()] ;

        for(int i=0; i< list.length ;i++)
        {
           list[i] = output2(getJsonMessage(URL), "regdate",i);
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,list);
        final ListView recordListView = (ListView)findViewById(R.id.recordListView);
        recordListView.setAdapter(adapter);

        //눌렀을때 동영상 재생.
        recordListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent playvideo = new Intent(Intent.ACTION_VIEW);
                playvideo.setDataAndType(Uri.parse(output2(getJsonMessage(URL), "path",position)), "video/mp4");
                startActivity(playvideo);
                //Toast.makeText(getBaseContext(), list[+position],Toast.LENGTH_SHORT).show();
            }
        });


    }

    public String getJsonMessage(String URL) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //String kidsyamUrl = "https://test-api.kidsyam.com:3443/testapi/test";

        String send="plz";

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(URL);

            get.setHeader("AuthKey", "c60690cc716b8ed0b8feb8333a59e98c");
            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();

            if (resEntityGet != null) {
                //do something with the response
                // Log.d("GET RESPONSE", EntityUtils.toString(resEntityGet));
                send = EntityUtils.toString(resEntityGet);
                //String send = URLEncoder.encode("한글","utf-8");  // 원래는 utf-8 처리를 하는게 맞음.
                return send;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return send;
    }

    //json 문구 다 가져오기.
    public String output(String input, String fistValue, String secondValut ){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            JSONArray jAr = new JSONArray("["+input+"]");
            //JSONArray jAr = new JSONArray(input);
            for(int i=0; i < jAr.length(); i++) {
                // 개별 객체를 하나씩 추출
                JSONObject obj1 = jAr.getJSONObject(i);
                JSONObject obj2 = obj1.getJSONObject(fistValue);
                // 객체에서 데이터를 추출
                strData += obj2.getString(secondValut) + "\n";
            }
        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }

    // 세부항목가져오기
    public String output2(String input, String fistValue ,int i){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            //  JSONArray jAr = new JSONArray("["+input+"]");
            JSONArray jAr = new JSONArray(input);
            // 개별 객체를 하나씩 추출
            JSONObject obj1 = jAr.getJSONObject(i);
            // 객체에서 데이터를 추출
            strData += obj1.getString(fistValue);

        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }

}
