package co.saru.familyyam;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class getKidsyamInfo extends AppCompatActivity {
    CookieStore cookieStore;
    HttpContext httpContext;

    String[] nameList = null;
    String[] sizeList = null;
    String[] stickerList = null;
    String[] r_stickerList = null;
    String[] imageList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String inputByJson = getKidsYamMessage("https://api.kidsyam.com/common/GetLoginInfo?Id=nadayoul@nate.com&Pw=123qwe&");
        String inputByJson2 = getKidsYamMessage("https://api.kidsyam.com/common/GetMapUseInfo?MapIdx=531");
        String inputByJson3 = getKidsYamMessage("https://test-api.kidsyam.com:3443/testapi/getListKidsYam?Creator=youl&Type=V");
        String inputByJson4 = getKidsYamMessage("https://api.kidsyam.com/common/GetLoginInfo?Id=gorao_ri@naver.com&Pw=tmddus4141");

        //tv.setText(previewChildInfo(getChildName(inputByJson4),getMapIdxList(inputByJson4)));
        //tv2.setText(getMapIdxList(inputByJson4));
        //tv2.setText(output2(inputByJson3,"regdate",0));
        //tv.setText(output(inputByJson4,"UserInfo","Name"));
        //tv2.setText(SessionControl.cookies.get(0).toString());
        //tv.setText(child_number(inputByJson4));


    }

    public String getKidsYamMessage(String URL) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //String kidsyamUrl = "https://test-api.kidsyam.com:3443/testapi/test";

        String send="plz";

        try {
            //HttpClient client = new DefaultHttpClient();
            HttpClient client = SessionControl.getHttpclient();  //세션유지
            HttpGet get = new HttpGet(URL);

            get.setHeader("AuthKey", "c60690cc716b8ed0b8feb8333a59e98c");
            HttpResponse responseGet = client.execute(get,httpContext);
            HttpEntity resEntityGet = responseGet.getEntity();



            if (resEntityGet != null) {
                //do something with the response
               // Log.d("GET RESPONSE", EntityUtils.toString(resEntityGet));
                String asdf;
                send = EntityUtils.toString(resEntityGet);
                SessionControl.cookies = SessionControl.httpclient.getCookieStore().getCookies(); // 세션유지.
                //String send = URLEncoder.encode("한글","utf-8");  // 원래는 utf-8 처리를 하는게 맞음.
                return send;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return send;
    }


    public String output(String input, String fistValue, String secondValut ){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            JSONArray jAr = new JSONArray("["+input+"]");
            //JSONArray jAr = new JSONArray(input);
            for(int i=0; i < jAr.length(); i++) {
                // 개별 객체를 하나씩 추출
                JSONObject obj1 = jAr.getJSONObject(i);
                JSONObject obj2 = obj1.getJSONObject(fistValue);
                // 객체에서 데이터를 추출
                strData += obj2.getString(secondValut);
            }
        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }

    //
    public String output2(String input, String fistValue ,int i){
        String strData = "";

        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            //  JSONArray jAr = new JSONArray("["+input+"]");
            JSONArray jAr = new JSONArray(input);
                // 개별 객체를 하나씩 추출
            JSONObject obj1 = jAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                strData += obj1.getString(fistValue);

        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }
        return strData;
    }


    public String getKidsYamMessage2(String URL) {


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        String send="plz";

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(URL);

            get.setHeader("AuthKey", "c60690cc716b8ed0b8feb8333a59e98c");
            get.setHeader((Header) httpContext);
            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();





            if (resEntityGet != null) {
                //do something with the response
                // Log.d("GET RESPONSE", EntityUtils.toString(resEntityGet));
                send = EntityUtils.toString(resEntityGet);
                //String send = URLEncoder.encode("한글","utf-8");  // 원래는 utf-8 처리를 하는게 맞음.
                return send;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return send;
    }

    // 아이의 맵정보 파싱.
    public String getMapIdxList(String inputByJson){
        String KidsInfo = "";
        String MapIdxList="";
        int KidsCount = 0 ;

        // 아이 목록 확인
        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            JSONArray jAr = new JSONArray("["+inputByJson+"]");
            // 개별 객체를 하나씩 추출
            for(int i=0; i < jAr.length(); i++) {
                JSONObject obj1 = jAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                KidsInfo += obj1.getString("KidsInfo");
            }
        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }

        try {
            JSONArray KidsjAr = new JSONArray(KidsInfo);
            KidsCount = KidsjAr.length();
            // 개별 객체를 하나씩 추출
            for(int i=0; i < KidsjAr.length(); i++) {
                JSONObject obj1 = KidsjAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                String[] beforePasing = obj1.getString("MapIdxList").split(",");
                String afterPasring = beforePasing[(beforePasing.length-1)];
                MapIdxList += afterPasring+" ";

            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return MapIdxList;
    }

    // 아이의 사진정보 파싱.
    public String getImageList(String inputByJson){
        String KidsInfo = "";
        String ImageList="";
        int KidsCount = 0 ;

        // 아이 목록 확인
        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            JSONArray jAr = new JSONArray("["+inputByJson+"]");
            // 개별 객체를 하나씩 추출
            for(int i=0; i < jAr.length(); i++) {
                JSONObject obj1 = jAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                KidsInfo += obj1.getString("KidsInfo");
            }
        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }

        try {
            JSONArray KidsjAr = new JSONArray(KidsInfo);
            KidsCount = KidsjAr.length();
            // 개별 객체를 하나씩 추출
            for(int i=0; i < KidsjAr.length(); i++) {
                JSONObject obj1 = KidsjAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                String[] beforePasing = obj1.getString("PhotoUrl").split(",");
                String afterPasring = beforePasing[(beforePasing.length-1)];
                ImageList += afterPasring+" ";

            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return ImageList;
    }


    // 아이의 맵정보 파싱.
    public String getChildName(String inputByJson){
        String KidsInfo = "";
        String ChildName="";
        int KidsCount = 0 ;

        // 아이 목록 확인
        try {
            // JSON 구문을 파싱해서 JSONArray 객체를 생성
            JSONArray jAr = new JSONArray("["+inputByJson+"]");
            // 개별 객체를 하나씩 추출
            for(int i=0; i < jAr.length(); i++) {
                JSONObject obj1 = jAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                KidsInfo += obj1.getString("KidsInfo");
            }
        } catch (JSONException e) {
            Log.d("tag", "Parse Error");
        }

        try {
            JSONArray KidsjAr = new JSONArray(KidsInfo);
            KidsCount = KidsjAr.length();
            // 개별 객체를 하나씩 추출
            for(int i=0; i < KidsjAr.length(); i++) {
                JSONObject obj1 = KidsjAr.getJSONObject(i);
                // 객체에서 데이터를 추출
                String beforePasing = obj1.getString("Name");
                ChildName += beforePasing+" ";

            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return ChildName;
    }



    public void previewChildInfo(String ChildName, String ChildMapIdxList, String ChildImageList){

        String[] arrChildName = ChildName.split(" ");
        String[] arrChildMapIdxList = ChildMapIdxList.split(" ");
        String[] arrChildImageList = ChildImageList.split(" ");

        String[] arrChildSize=new String[arrChildName.length];
        String[] arrChildSticker=new String[arrChildName.length];
        String[] arrChildR_sticker=new String[arrChildName.length];
        String[] arrImageAddress=new String[arrChildName.length];


        for(int i=0;i<arrChildName.length;i++) {
            if(arrChildMapIdxList[i].equals("null")){
                arrChildSize[i] = "";
                arrChildSticker[i] = "스티커판을 생성해주세요.              ";
                arrChildR_sticker[i] = "";
                arrImageAddress[i]="null";
            }

            else{
                String inputByJson = getKidsYamMessage("https://api.kidsyam.com/common/GetMapUseInfo?MapIdx=" + arrChildMapIdxList[i]);

                String usedStickerInfo = "";
                String usedStickernumber = "";

                try {
                    // JSON 구문을 파싱해서 JSONArray 객체를 생성
                    JSONArray StickerjAr = new JSONArray("[" + inputByJson + "]");
                    // 개별 객체를 하나씩 추출
                    for (int j = 0; j < StickerjAr.length(); j++) {
                        JSONObject obj1 = StickerjAr.getJSONObject(j);
                        // 객체에서 데이터를 추출
                        usedStickerInfo += obj1.getString("StickerUseInfo");
                    }
                } catch (JSONException e) {
                    Log.d("tag", "Parse Error");
                }

                try {
                    JSONArray KidsjAr = new JSONArray(usedStickerInfo);
                    usedStickernumber = String.valueOf(KidsjAr.length());
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }


//            output += "이름 : "+arrChildName[i]+"\n"+"스티커판 크기: "+output(inputByJson,"MapInfo","SlotCnt")+"\n"
//            +"붙인 스티커수 : "+usedStickernumber+"\n"+"남은 스티커수 : "+String.valueOf(Integer.parseInt(output(inputByJson,"MapInfo","SlotCnt"))-Integer.parseInt(usedStickernumber))+"\n\n";

               // arrImageAddress[i] = output2(,"PhotoUrl",0);
                arrChildSize[i] = output(inputByJson, "MapInfo", "SlotCnt");
                arrChildSticker[i] = "붙인 스티커 : " + usedStickernumber;
                Log.d("######",arrChildMapIdxList[i]);
                arrChildR_sticker[i] = "남은 스티커 : " + String.valueOf(Integer.parseInt(output(inputByJson, "MapInfo", "SlotCnt")) - Integer.parseInt(usedStickernumber));


            }

            setImageList(arrChildImageList);
            setNameList(arrChildName);
            setSizeList(arrChildSize);
            setStickerList(arrChildSticker);
            setr_stickerList(arrChildR_sticker);
        }
    }


    public void setNameList(String[] Namelist){
        this.nameList = Namelist;
    }
    public String[] getNameList(){
        return nameList;
    }

    public void setSizeList(String[] Sizelist){
        this.sizeList = Sizelist;
    }
    public String[] getSizeList(){
        return sizeList;
    }


    public void setStickerList(String[] Stickerlist){
        this.stickerList = Stickerlist;
    }
    public String[] getStickerList(){return stickerList;}

    public void setr_stickerList(String[] R_stickerList){
        this.r_stickerList = R_stickerList;
    }
    public String[] getr_stickerList(){
        return r_stickerList;
    }

    public void setImageList(String[] ImageList){this.imageList = ImageList;}
    public String[] getImageList(){
        return imageList;
    }


}
