package co.saru.familyyam;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;

/**
 * Created by saru on 2016. 7. 29..
 */
public class showRecordList extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recordlist);


        final File localPath = new File(Environment.getExternalStorageDirectory()+ "/DCIM/familyYam/") ;
        final File[] fileList = localPath.listFiles();

        final String[] list = new String[fileList.length] ;

        for(int i=0; i< fileList.length ;i++)
        {
            list[i] = fileList[fileList.length-i-1].getName().toString();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,list);
        final ListView recordListView = (ListView)findViewById(R.id.recordListView);
        recordListView.setAdapter(adapter);
        



       // 리스트 눌렸을때 동영상 실행
        recordListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent playvideo = new Intent(Intent.ACTION_VIEW);
                File videoLocation = new File(Environment.getExternalStorageDirectory()+ "/DCIM/familyYam/"+list[+position]);
                playvideo.setDataAndType(Uri.fromFile(videoLocation), "video/mp4");
                startActivity(playvideo);
                //Toast.makeText(getBaseContext(), list[+position],Toast.LENGTH_SHORT).show();
            }
        });


        //리스트 길게 눌렀을때 메세지 띄움
        recordListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "전송중입니다..."+list[position], Toast.LENGTH_SHORT).show();


                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                String path = Environment.getExternalStorageDirectory() + "/DCIM/familyYam/"+list[position];
                File file = new File(path);
                try {
                    String Creator = "parent";
                    Log.d("@@@@@@@@@",list[position]);
                    String Title = list[position];
                    String Message = "parent_message";

                    HttpClient client = new DefaultHttpClient();
                    String postURL = "https://test-api.kidsyam.com:3443/test/upload?Creator="+Creator+"&Title="+Title+"&Message="+Message+"&Type=V";
                    HttpPost post = new HttpPost(postURL);
                    FileBody bin = new FileBody(file);
                    MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                    reqEntity.addPart("PhotoData", bin);
                    post.setEntity(reqEntity);
                    HttpResponse response = client.execute(post);
                    HttpEntity resEntity = response.getEntity();
                    if (resEntity != null) {
                        Log.i("RESPONSE@@@@", EntityUtils.toString(resEntity));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true; //true로 해야 숏이 안뜸.
            }
        });


    }
}